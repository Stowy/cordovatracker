// Dom7
var $$ = Dom7;

// Framework7 App main instance
var app = new Framework7({
    root: "#app", // App root element
    id: "tracker.fabianhuber.trackercordova", // App bundle ID
    name: "Tracker", // App name
    theme: "auto", // Automatic theme detection
    // App root data
    data: function () {
        return {};
    },
    // App root methods
    methods: {},
    // App routes
    routes: routes,
});

// Init/Create views
var homeView = app.views.create("#view-home", {
    url: "/",
});
var settingsView = app.views.create("#view-settings", {
    url: "/settings/",
});

// Login Screen Demo
$$("#my-login-screen .login-button").on("click", function () {
    var username = $$('#my-login-screen [name="username"]').val();
    var password = $$('#my-login-screen [name="password"]').val();

    // Close login screen
    app.loginScreen.close("#my-login-screen");

    // Alert username and password
    app.dialog.alert("Username: " + username + "<br>Password: " + password);
});