/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
document.addEventListener('deviceready', onDeviceReady, false);

const TABLE_NAME = "PositionsHistory";

let intervalTime = 25000;
let db = openDatabase("Tracker_DB", "1.0", "DB of the tracker project", 5 * 1024 * 1024);

function onDeviceReady() {
    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    
    createDB();
    loadAndDisplayPosition();

    function updatePosition() {
        navigator.geolocation.getCurrentPosition(getPositionSucess, getPositionError);

        // Call the update again
        setTimeout(updatePosition, intervalTime);
    }

    // Calls the updatePosition for the first time
    setTimeout(updatePosition, intervalTime);
}

/**
 * Creates the PositionHistory table if it does not exist.
 */
function createDB(){
    db.transaction(function(tx){
        tx.executeSql(`CREATE TABLE IF NOT EXISTS ${TABLE_NAME} (
            latitude TEXT NOT NULL,
            longitude TEXT NOT NULL,
            timestamp TEXT)`);
    });
}

function loadAndDisplayPosition() {
    // Load the position
    db.transaction(function(tx){
        tx.executeSql(`SELECT * FROM ${TABLE_NAME} ORDER BY timestamp DESC LIMIT 1`, [], function(tx, results){
            $$('#position').text(`Latitude : ${results.rows.item(0).latitude} \nLongitude : ${results.rows.item(0).longitude}`);
            $$("#position-timestamp").text("Timestamp: " + results.rows.item(0).timestamp);
            drawMap(results.rows.item(0).latitude, results.rows.item(0).longitude);
        });
    });
}

function loadUserSettings() {
    intervalTime = localStorage.getItem("positionRefreshTime");
    app.range.setValue("#refreshFrequency", intervalTime / 1000)
    $$("#refreshFrequencyValue").html("The position will refresh every " + intervalTime / 1000 + "s");
}

function getPositionSucess(position) {
    // Display the position
    $$('#position').text(`Latitude : ${position.coords.latitude}\nLongitude : ${position.coords.longitude}`);

    // Display position status
    $$("#positionStatus").text("Position is known.");

    // Display timestamp
    $$("#position-timestamp").text("Timestamp: " + position.timestamp);

    drawMap(position.coords.latitude, position.coords.longitude);

    // Save the position
    db.transaction(function(tx){
        tx.executeSql(`INSERT INTO ${TABLE_NAME} (latitude, longitude, timestamp) 
        VALUES ("${position.coords.latitude}", "${position.coords.longitude}", "${position.timestamp}")`);
    });
}

function drawMap(latitude, longitude){
    let width = Math.floor(window.innerWidth * 0.9);
    let height = Math.floor(window.innerHeight * 0.4);
    let url = `https://maps.geoapify.com/v1/staticmap?style=osm-carto&width=${width}&height=${height}&center=lonlat:${longitude},${latitude}&zoom=12&apiKey=e72c4da1abab455bb316a5b1fc699715`
    let image = $$("#map-position");
    image.attr("src", url);
    image.css("visibility", "visible");
    image.attr("width", width);
    image.attr("height", height);
}

function getPositionError() {
    // Display position status
    $$("#positionStatus").text("Position is unknown.");
}

function updateRefreshFrequencyValue() {
    // Read the value
    let refreshValue = app.range.getValue("#refreshFrequency");

    // Update the display
    $$("#refreshFrequencyValue").html("The position will refresh every " + refreshValue + "s");

    // Update the var
    intervalTime = refreshValue * 1000;

    // Save setting
    localStorage.setItem("positionRefreshTime", intervalTime);
}